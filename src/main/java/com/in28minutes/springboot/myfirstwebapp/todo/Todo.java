package com.in28minutes.springboot.myfirstwebapp.todo;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.Size;

import java.time.LocalDate;
import java.util.Objects;

@Entity
public class Todo implements Comparable<Todo> {

    @Id
    @GeneratedValue
    private int id;
    private String userName;
    @Size(min = 3, message = "Enter at least 3 characters")
    private String description;
    private LocalDate targetDate;
    private boolean done;

    public Todo() {
    }

    public Todo(int id, String userName, String description, LocalDate targetDate, boolean done) {
        this.id = id;
        this.userName = userName;
        this.description = description;
        this.targetDate = targetDate;
        this.done = done;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(LocalDate targetDate) {
        this.targetDate = targetDate;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", description='" + description + '\'' +
                ", targetDate=" + targetDate +
                ", done=" + done +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof Todo todo))
            return false;

        return Objects.equals(this.id, todo.id) &&
                Objects.equals(this.userName, todo.userName) &&
                Objects.equals(this.description, todo.description) &&
                Objects.equals(this.targetDate, todo.targetDate) &&
                Objects.equals(this.done, todo.done);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, description, targetDate, done);
    }

    @Override
    public int compareTo(Todo todo) {
        return Integer.compare(this.id, todo.id);
    }
}
