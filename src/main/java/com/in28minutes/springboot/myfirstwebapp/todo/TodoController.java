package com.in28minutes.springboot.myfirstwebapp.todo;

import jakarta.validation.Valid;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.time.LocalDate;

//@Controller
@SessionAttributes("name")
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    // /list-todos endpoint
    @RequestMapping("/list-todos")
    public String listAllTodos(ModelMap model) {
        model.addAttribute("todos", todoService.findByUserName(getLoggedInUserName(model)));
        return "listTodos";
    }

    @RequestMapping(value = "add-todo", method = RequestMethod.GET)
    public String showAddTodoView(ModelMap model) { //Binding from Controller to View
        model.put("todo", new Todo(0, getLoggedInUserName(model), "", LocalDate.of(2023, 6, 30), false));
        return "todo";
    }

    @RequestMapping(value = "add-todo", method = RequestMethod.POST)
    public String addTodo(ModelMap model, @Valid Todo todo, BindingResult result) { //Binding from View to Controller

        if (result.hasErrors())
            return "todo";

        todoService.insertTodo(getLoggedInUserName(model), todo.getDescription(), todo.getTargetDate(), false);
        // When you want to redirect to a specific URL, specify the name of the URL, not the name of JSP file.
        return "redirect:list-todos";
    }

    @RequestMapping("/delete-todo")
    public String deleteTodo(@RequestParam int id) {
        todoService.deleteById(id);
        return "redirect:list-todos";
    }

    @RequestMapping(value = "update-todo", method = RequestMethod.GET)
    public String showUpdateTodoView(@RequestParam int id, ModelMap model) {
        model.addAttribute("todo", todoService.findById(id));
        return "todo";
    }

    @RequestMapping(value = "update-todo", method = RequestMethod.POST)
    public String updateTodo(ModelMap model, @Valid Todo todo, BindingResult result) {

        if (result.hasErrors())
            return "todo";

        todo.setUserName(getLoggedInUserName(model));
        todoService.updateTodo(todo);
        return "redirect:list-todos";
    }

    private static String getLoggedInUserName(ModelMap model) {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

}
